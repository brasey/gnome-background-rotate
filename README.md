# gnome-background-rotate

Change your Gnome background to a random image in a directory.

## Usage

You have to have Go installed at this point.

```bash
go run background.go
```
