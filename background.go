package main

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"os/exec"
	"time"
)

var dir string = "/home/brasey/Pictures/scifi"

func main() {
	var filenames []string

	files, err := ioutil.ReadDir(dir)
	if err != nil {
		fmt.Println("Couldn't read directory")
		return
	}

	for _, file := range files {
		filenames = append(filenames, file.Name())
	}

	rand.Seed(time.Now().UnixNano())
	background := dir + "/" + filenames[rand.Intn(len(filenames))]

	fmt.Printf("Changing background to %v.\n", background)
	cmd := exec.Command("gsettings", "set", "org.gnome.desktop.background", "picture-uri", "file://"+background)
	err = cmd.Run()

	if err != nil {
		fmt.Printf("Command finished with error: %v\n", err)
	}
}
